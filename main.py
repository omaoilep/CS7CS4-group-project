import SVM.SVMModel as plugin
from utility import *
from math import sqrt

DATA_POINT_MINIMUM = 100
DATA_POINT_MAXIMUM = 11000
DATA_POINT_STEP = 200
ITERATION_LIMIT = 5


def main():
    print('Initialising Data Set...')
    print(f'Reading in {plugin.DATA_SET_FILE}...')
    data_set = np.loadtxt(plugin.DATA_SET_FILE,  delimiter=plugin.DELIMITER)
#    data_set = np.column_stack((np.ones((data_set.shape[0], 1)), data_set))

    print('Creating Training and Testing Sets...')

    if plugin.TEST_DATA_SET_FILE is not None:
        training_data = data_set
        testing_data = np.loadtxt(plugin.TEST_DATA_SET_FILE, delimiter=plugin.TEST_DELIMITER)
    else:
        training_data, testing_data = randomly_split_data(data_set, testing_percentage=10)
        
#    testing_data = [list(elem) for elem in testing_data]
#    testing_data = np.asarray(testing_data)
#    training_data = [list(elem) for elem in training_data]
#    training_data = np.asarray(training_data)
    
    testing_data_x = testing_data[:, :testing_data.shape[1] - 1]
    testing_data_y = testing_data[:, testing_data.shape[1] - 1:]

    final_results = list()
    for data_point_amount in range(DATA_POINT_MINIMUM, DATA_POINT_MAXIMUM+1, DATA_POINT_STEP):
        if data_point_amount < len(training_data):

            best_result_list = list()

            for i in range(1, ITERATION_LIMIT+1):
                print(f'Data Point: {data_point_amount},\t Iteration {i}')

                sampled_training_data = training_data[np.random.choice(training_data.shape[0], data_point_amount, replace=False)]

                sampled_training_data_x = sampled_training_data[:, :sampled_training_data.shape[1] - 1]
                sampled_training_data_y = sampled_training_data[:, sampled_training_data.shape[1] - 1:]

                full_reports_list = plugin_initialiser(sampled_training_data_x, sampled_training_data_y, testing_data_x, testing_data_y)
                best_result = plugin.choose_best_result(full_reports_list)
                best_result['data_point_amount'] = data_point_amount
                best_result_list.append(best_result)

            final_results.append(best_result_list)
    write_out_results(final_results)
    return


def plugin_initialiser(training_data_x, training_data_y, testing_data_x, testing_data_y):
    result_list = []

    extended_training_data_x = generate_extra_columns(training_data_x, plugin.FEATURE_LIMIT)
    extended_testing_data_x = generate_extra_columns(testing_data_x, plugin.FEATURE_LIMIT)

    starting_feature_amount = training_data_x.shape[1]

    for i in range(starting_feature_amount, plugin.FEATURE_LIMIT+1):

        current_training_data = extended_training_data_x[:, :i]
        result_dicts = plugin.plugin_wrapper(current_training_data, training_data_y)

        for result_dict in result_dicts:
            model = result_dict['model']
            metrics = plugin.model_tester(model, extended_testing_data_x[:, :i], testing_data_y)

            full_result_dict = {**result_dict, **metrics}
            result_list.append(full_result_dict)

    return result_list


def generate_extra_columns(data, return_length):
    starting_length = data.shape[1]
    extended_data_set = data
    current_power = 2
    current_original_column = 1
    for i in range(starting_length, return_length):

        extracted_column = extended_data_set[:, [current_original_column]]
        new_column = np.power(extracted_column, current_power)
        extended_data_set = np.column_stack((extended_data_set, new_column))
        current_original_column = current_original_column + 1
        if current_original_column == starting_length:
            current_original_column = 1
            current_power = current_power + 1

    return extended_data_set


def write_out_results(results):
    with open('all_results.csv', 'w') as all_results, open('stand_dev_result.csv', 'w') as stand_dev_file:
        for result_list in results:
            data_point_amount = result_list[0]['data_point_amount']
            scores = [x['score'] for x in result_list]
            mean = sum(scores)/len(scores)
            mean_diff = [score - mean for score in scores]
            mean_diff = [diff*diff for diff in mean_diff]
            stand_dev = sum(mean_diff)/len(mean_diff)
            stand_dev = sqrt(stand_dev)
            for score in scores:
                all_results.write(f'{data_point_amount},{score}\n')
            stand_dev_file.write(f'{data_point_amount},{stand_dev}\n')


if __name__ == '__main__':
    main()
