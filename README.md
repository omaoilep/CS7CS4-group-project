Object Overview:

Full Data Report:
A tuple containing in the following order
(Model, Hyper_Parameters, Error_Rate, Accuracy, Precision, Recall)

Model:
A list of variable length of theta values
[θ1, θ2, .... θn]

Hyper_Parameters:
A list of Hyper Parameters entered into the machine learning algorithm.
For now the only Hyper Parameters are the learning rate alpha and the Holdout generation limit
[α, N]

Error_Rate, Accuracy, Precision, Recall:
Evaluation Metrics, formulas can be found in Joeran's slides. 
All metrics should be floats


Function Overviews:
Main(Data_Set) --> Full Data Report
    -Takes in Data Set
    - Splits Data into Training and Testing
    - Passes X amount of Training points and Testing Data into Model_Initialiser
    - Takes Results of Initiliser and passes them into the ResultsComparer 
    - Takes Result of Result Comparer and returns it
    
Model_Initialiser(Test_Data, Training_Data) --> [Full Data Report]
    - Has list of Model Configurations
    - Configures Test and Training Data for each Model Configuration
        (i.e. Turning [x1, x2] data into [x1, x2, x1^2])
    - Passes Configured Training Data and starting Model into Holdout_Wrapper
    - Takes Results of each Holdout Wrapper run and passes model into the Model Tester
    - Makes Full Report Spec from the result of Holdout_Wrapper and the result 
        Model Tester
    -Returns list of Full Report Specs

Holdout_Wrapper(Training_Data, Start_Model) --> [(Model, Hyper_Parameter)]
    - Generates a List of Models using the Training Data and the Start Model by 
        iterating through different Hyper Parameter Configurations
    - Returns list of tuples containg the Model and Hyper Parameter Configuration
    
Holdout(Training_Data, Start_Model, Hyper_Parameters) --> Model
    - Splits Training Data into Training and Validation sets
    - Performs one iteration of Gradient Descent and then tests the Model on the
        Validation set, the error rate is stored
    - Repeats this process until generation limit is hit.
    - Returns Model with the lowest error rate 
    
Model_Tester(Model, Test_Data) --> (Error_Rate, Accuracy, Precision, Recall)
    - Takes Model and Data and calculates predictions 
    - Compares predictions with Data
    - Returns Metrics
    
Results_Comparer([Full Data Report]) --> Full Data Report
    - Takes a list of Full Data Reports and examines the result Metrics
    - Returns the best Full Data Report
    - Exact workings and comparisons not yet set. Let's leave this until Wednesday