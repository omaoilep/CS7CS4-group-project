import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

DATA_SET_FILE_2 = 'LinearRegression/winequality-white.csv'
DATA_SET_FILE = 'LinearRegression/carbon_nanotubes.csv'
TEST_DATA_SET_FILE_2 = 'LinearRegression/winequality-red.csv'
TEST_DATA_SET_FILE = None
DELIMITER_2 = ';'
TEST_DELIMITER_2 = ';'
DELIMITER = ';'
TEST_DELIMITER = ','
FEATURE_LIMIT = 15

def plugin_wrapper(x, y):
    alpha = [0.01, 0.05, 0.1, 0.3, 0.5, 0.7, 0.9, 1]
    result_dicts = []
    for a in alpha:
        result_dict = dict()
        model = holdout(x, y, a)
        result_dict['model'] = model
        result_dict['hypers'] = [a]
        result_dicts.append(result_dict)
    return result_dicts

def holdout(x, y, alpha, generations=10000):
    y = y.flatten()
    model = LinearRegression().fit(x,y)
    return model

def predict(x, theta):
    pred = np.matmul(x, theta)
    return np.sign(pred)

def model_tester(model, testing_data, testing_answers):
    pred = model.predict(testing_data)
    pred = np.reshape(pred, (len(pred), 1))
    meanSE = mean_squared_error(testing_answers, pred)

    metrics = {'mean': meanSE}
    return metrics

def choose_best_result(reports):
    scores = list()
    for report in reports:
        meanSE = report['mean']
        scores.append(meanSE)

    min_score, min_index = min((min_score, min_index) for (min_index, min_score) in enumerate(scores))

    reports[min_index]['score'] = min_score

    return reports[min_index]
