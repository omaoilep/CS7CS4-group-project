import numpy as np
from sklearn import svm
from sklearn import metrics

DATA_SET_FILE = 'SVM/avila-tr.csv'
DELIMITER = ','
TEST_DATA_SET_FILE = 'SVM/avila-ts.csv'
TEST_DELIMITER = ','
FEATURE_LIMIT = 10

def plugin_wrapper(x, y):
    alpha = [1, 10, 100]
    result_dicts = []
    for a in alpha:
        print(f'Generating Model when alpha = {a}')
        result_dict = dict()
        model = holdout(x, y, a)
        result_dict['model'] = model
        result_dict['hypers'] = [a]
        result_dicts.append(result_dict)
    return result_dicts


def holdout(x, y, alpha, generations=100000):
    model = svm.SVC(C=alpha, max_iter=generations, gamma='scale')
    y = np.ravel(y)
    model.fit(x, y)
    return model
	
def model_tester(model, testing_data, testing_answers):
    pred = model.predict(testing_data)
    testing_answers = np.ravel(testing_answers)
#     acc = metrics.accuracy_score(pred, testing_answers)
#     recall = metrics.recall_score(pred, testing_answers, average=None)
    av_prec = metrics.precision_score(testing_answers, pred, average='macro')
    f1_score = metrics.f1_score(testing_answers, pred, average='macro')
# 
#     metric_scores = {'acc': acc, 'prec': prec, 'recall': recall}

    metric_scores = {'av_prec' : av_prec, 'f1_score' : f1_score}
    return metric_scores


def choose_best_result(reports):
    max_score, max_index = max((max_score, max_index) for (max_index, max_score) in enumerate([report['f1_score'] for report in reports]))

    best_report = reports[max_index]
    best_report['score'] = max_score

    return best_report