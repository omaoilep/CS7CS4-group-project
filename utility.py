import numpy as np


def randomly_split_data(data_set, testing_percentage=0):
    shuffled_data = data_set
    np.random.shuffle(shuffled_data)
    split_point = round((len(data_set) * testing_percentage) / 100)
    training_data = shuffled_data[split_point:, :]
    testing_data = shuffled_data[:split_point, :]
    return training_data, testing_data
