import numpy as np
from sklearn.linear_model import LogisticRegression

DATA_SET_FILE = 'LogisticRegression/dota2All.csv'
DELIMITER = ','
FEATURE_LIMIT = 124
TEST_DATA_SET_FILE = None

def plugin_wrapper(x, y):
    alpha = [0.01, 0.05, 0.1, 0.3, 0.5, 0.7, 0.9, 1]
    result_dicts = []
    for a in alpha:
        result_dict = dict()
        model = holdout(x, y, a)
        result_dict['model'] = model
        result_dict['hypers'] = [a]
        result_dicts.append(result_dict)
    return result_dicts


def holdout(x, y, alpha, generations=10000):
    y = y.flatten()
    model = LogisticRegression(solver='lbfgs', C=alpha, max_iter=generations)
    model.fit(x, y)
    return model


def predict(x, theta):
    pred = np.matmul(x, theta)
    return np.sign(pred)


def model_tester(model, testing_data, testing_answers):
    pred = model.predict(testing_data)
    pred = np.reshape(pred, (len(pred), 1))
    diff = np.subtract(testing_answers, pred)
    sum = np.add(testing_answers, pred)

    tp = np.count_nonzero(sum == 2)
    fp = np.count_nonzero(diff == -2)
    tn = np.count_nonzero(sum == -2)
    fn = np.count_nonzero(diff == 2)

    # Accuracy
    acc = (tp + tn) / (tp + tn + fp + fn)

    # Precision
    prec = 0
    if tp + fp is not 0:
        prec = tp / (tp + fp)

    # Recall
    recall = 0
    if tp + fn is not 0:
        recall = tp / (tp + fn)

    metrics = {'acc': acc, 'prec': prec, 'recall': recall}
    return metrics


def choose_best_result(reports):
    scores = list()
    for report in reports:
        prec = report['prec']
        recall = report['recall']
        f_score = 0
        if prec + recall != 0:
            f_score = (2 * (prec * recall)) / (prec + recall)
        scores.append(f_score)

    max_score, max_index = max((max_score, max_index) for (max_index, max_score) in enumerate(scores))

    reports[max_index]['score'] = max_score

    return reports[max_index]
