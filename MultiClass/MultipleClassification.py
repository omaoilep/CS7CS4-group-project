from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC


DATA_SET_FILE = 'MultiClass/Pokerhands/pokerHands.csv'
DELIMITER = ','
TEST_DATA_SET_FILE = 'MultiClass/Pokerhands/pokerHandsTest.csv'
TEST_DELIMITER = ','
FEATURE_LIMIT = 11
CLASS_AMOUNT = 10


def plugin_wrapper(x, y):
    model = OneVsRestClassifier(LinearSVC(random_state=0, max_iter=100000)).fit(x, y)
    model_dict = {'model': model}
    results = list()
    results.append(model_dict)
    return results


def model_tester(model, testing_data, testing_answers):
    pred = model.predict(testing_data)

    tp_scores = [0] * CLASS_AMOUNT
    fp_scores = [0] * CLASS_AMOUNT

    for i in range(len(pred)):
        if pred[i] == testing_answers[i]:
            tp_index = int(testing_answers[i]-1)
            tp_scores[tp_index] = tp_scores[tp_index] + 1
        else:
            fp_index = int(pred[i]-1)
            fp_scores[fp_index] = fp_scores[fp_index] + 1

    prec_scores = list()
    for i in range(len(tp_scores)):
        prec = 0
        if tp_scores[i] != 0 or fp_scores[i] != 0:
            prec = tp_scores[i]/(tp_scores[i]+fp_scores[i])
        prec_scores.append(prec)

    av_prec = sum(prec_scores) / len(prec_scores)

    metrics = {'av_prec' : av_prec}
    return metrics


def choose_best_result(reports):
    max_score, max_index = max((max_score, max_index) for (max_index, max_score) in enumerate([report['av_prec'] for report in reports]))

    best_report = reports[max_index]
    best_report['score'] = max_score

    return best_report
